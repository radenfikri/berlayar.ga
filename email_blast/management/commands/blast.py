from django.core.management.base import BaseCommand
from django.utils import timezone
from email_blast import views

class Command(BaseCommand):
    help = 'Blast email sesuai email di database'

    def handle(self, *args, **kwargs):
        views.blast()