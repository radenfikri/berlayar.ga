
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('submit-email', views.submit, name='submit-email'),
]
