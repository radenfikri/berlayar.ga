from django.shortcuts import render
import requests
import json
from .models import EmailSubscriber

from datetime import datetime
from django.http import HttpResponse
from django.http import HttpResponseRedirect as redirect
from django.core.mail import send_mail
import datetime
from weathers import views




# Create your views here.
def index(request):
    return render(request, 'form.html')

def submit(request):
    if request.method == 'POST':
        if (EmailSubscriber.objects.all().filter(email=request.POST["email"]).count()==0):
            EmailSubscriber.objects.create( name = request.POST['nama'], email = request.POST['email'])
            send_mail('Berlayar ga ' + request.POST["nama"], 'THANK YOU FOR SUBSCRIBING, now you will receive email every morning at 7:00 am that contains Berlayar.ga information.', 'Berlayar.ga', [request.POST["email"]], fail_silently=False)
        return redirect('/main-page/')
    else:
        return render(request, 'form.html')

def blast():
    if (views.randomize()):
        for x in EmailSubscriber.objects.all():
            send_mail('Berlayar ga', 'Selamat Pagi, informasi Berlayar.ga pagi ini tanggal ' + datetime.datetime.now().strftime ("%d/%m/%Y") +' AMAN Untuk Berlayar! Silahkan mencari ikan ikan dilautan indah!', 'Berlayar.ga', [x.email], fail_silently=False)
    else:
        for x in EmailSubscriber.objects.all():
            send_mail('Berlayar ga', 'Selamat Pagi, informasi Berlayar.ga pagi ini tanggal ' + datetime.datetime.now().strftime ("%d/%m/ %Y") +' TIDAK AMAN Untuk Berlayar! JANGANLAH anda berlayar hari ini!', 'Berlayar.ga', [x.email], fail_silently=False)
