from django.conf.urls import url
from .views import index
from .views import check_info

app_name = 'main_page'

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'check_info/$', check_info, name='check_info'),
 ]
