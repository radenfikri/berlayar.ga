from django.shortcuts import render, redirect
from .models import InfoLayar
from weathers.views import randomize
import json

info_dict = {}


def index(request):
    return render(request, 'berlayarga/layout/base.html')


def check_info(request):
    good = randomize()
    if good == 1:
        # info_dict = InfoLayar.objects.filter(info="Yeay! hari ini aman")
        return render(request, 'berlayarga/layout/base.html', {'info_dict': "Yeay! hari ini aman"})
        # InfoLayar.objects.create(info=request.POST['info'])
        # return redirect('/main-page/')
    else:
        # info_dict = InfoLayar.objects.filter(
        #     info="Yah! hari ini tidak aman")
        return render(request, 'berlayarga/layout/base.html', {'info_dict': "Yah! hari ini tidak aman"})
        # InfoLayar.objects.create(info=request.POST['info'])


def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
