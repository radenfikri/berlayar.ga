# Generated by Django 2.1.3 on 2018-11-28 19:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weathers', '0003_auto_20181128_0547'),
    ]

    operations = [
        migrations.AddField(
            model_name='weather',
            name='max_temp',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='weather',
            name='min_temp',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
