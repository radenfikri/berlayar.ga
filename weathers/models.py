from django.db import models


class WeatherManager(models.Manager):
    def create_weather(self, station_number, pub_date,
                       min_temp,
                       mean_temp,
                       max_temp,
                       humidity,
                       rainfall,
                       radiation,
                       mean_vel,
                       wind_dir,
                       max_vel,
                       dir_when_max):
        weather = self.create(station_number=station_number, pub_date=pub_date, min_temp=min_temp, mean_temp=mean_temp, max_temp=max_temp, humidity=humidity,
                              rainfall=rainfall, radiation=radiation, mean_vel=mean_vel, wind_dir=wind_dir, max_vel=max_vel, dir_when_max=dir_when_max)
        # do something with the book
        return weather

    def get_temp(self):
        cold_temp = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).count()
        normal_temp = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).count()

        cold_prob = cold_temp/(cold_temp+normal_temp)
        normal_prob = normal_temp/(cold_temp+normal_temp)

        return dict({'-T': cold_prob, 'T': normal_prob})

    def get_humidity(self):
        lo_humid_lo_temp = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).count()
        hi_humid_lo_temp = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).count()
        lo_humid_hi_temp = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).count()
        hi_humid_hi_temp = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).count()

        lhlt = lo_humid_lo_temp/(lo_humid_lo_temp+hi_humid_lo_temp)
        hhlt = hi_humid_lo_temp/(lo_humid_lo_temp+hi_humid_lo_temp)
        lhht = lo_humid_hi_temp/(lo_humid_hi_temp+hi_humid_hi_temp)
        hhht = hi_humid_hi_temp/(lo_humid_hi_temp+hi_humid_hi_temp)

        return dict({
            "H|-T": hhlt,
            "-H|-T": lhlt,
            "H|T": hhht,
            "-H|T": lhht
        })

    def get_rainfall(self):
        lo_rainfall_lhlt = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).filter(rainfall__lt=50).count()
        lo_rainfall_hhlt = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).filter(rainfall__lt=50).count()
        lo_rainfall_lhht = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).filter(rainfall__lt=50).count()
        lo_rainfall_hhht = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).filter(rainfall__lt=50).count()
        hi_rainfall_lhlt = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).filter(rainfall__gte=50).count()
        hi_rainfall_hhlt = self.filter(min_temp__lte=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).filter(rainfall__gte=50).count()
        hi_rainfall_lhht = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__lt=80).filter(rainfall__gte=50).count()
        hi_rainfall_hhht = self.filter(min_temp__gt=23).filter(
            max_temp__lt=39).filter(humidity__gte=80).filter(rainfall__gte=50).count()

        lrlhlt = lo_rainfall_lhlt/(lo_rainfall_lhlt+hi_rainfall_lhlt)
        lrhhlt = lo_rainfall_hhlt/(lo_rainfall_hhlt+hi_rainfall_hhlt)
        lrlhht = lo_rainfall_lhht/(lo_rainfall_lhht+hi_rainfall_lhht)
        lrhhht = lo_rainfall_hhht/(lo_rainfall_hhht+hi_rainfall_hhht)
        hrlhlt = hi_rainfall_lhlt/(lo_rainfall_lhlt+hi_rainfall_lhlt)
        hrhhlt = hi_rainfall_hhlt/(lo_rainfall_hhlt+hi_rainfall_hhlt)
        hrlhht = hi_rainfall_lhht/(lo_rainfall_lhht+hi_rainfall_lhht)
        hrhhht = hi_rainfall_hhht/(lo_rainfall_hhht+hi_rainfall_hhht)

        return dict({
            "-R|H|-T": lrhhlt,
            "-R|-H|-T": lrlhlt,
            "-R|H|T": lrhhht,
            "-R|-H|T": lrlhht,
            "R|H|-T": hrhhlt,
            "R|-H|-T": hrlhlt,
            "R|H|T": hrhhht,
            "R|-H|T": hrlhht,
        })


class Weather(models.Model):
    station_number = models.CharField(max_length=6)
    pub_date = models.DateField()
    min_temp = models.FloatField()
    mean_temp = models.FloatField()
    max_temp = models.FloatField()
    humidity = models.IntegerField()
    rainfall = models.FloatField()
    radiation = models.FloatField()
    mean_vel = models.IntegerField()
    wind_dir = models.CharField(max_length=2)
    max_vel = models.IntegerField()
    dir_when_max = models.IntegerField()

    objects = WeatherManager()


class ProbabilityManager(models.Manager):
    def create_prob(self, symbol, probability):
        probability = self.create(symbol=symbol, probability=probability)
        # do something with the book
        return probability


class Probability(models.Model):
    symbol = models.CharField(max_length=200)
    probability = models.FloatField()

    objects = ProbabilityManager()
