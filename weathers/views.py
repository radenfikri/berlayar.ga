import requests
import json
import re
import psycopg2
import os

from datetime import datetime
from random import choices
from django.shortcuts import render
from django.http import HttpResponse
from .models import Weather, Probability


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def get_data(request):
    Weather.objects.all().delete()
    r = requests.post('https://dataonline.bmkg.go.id/data_iklim/get_data', data={
                      'idrefprovince': 13, 'idrefregency': 184, 'startdate': '01/01/2008', 'enddate': datetime.now()})

    data = r.json()['data']
    invalid_val = [8888, 9999]

    for w in data:
        filtered = [d for d in w if d not in invalid_val]

        if len(filtered) == 13:
            datetime_object = datetime.strptime(w[2], '%d-%m-%Y')
            weather = Weather.objects.create_weather(
                w[0], datetime_object, w[3], w[5], w[4], w[6], w[7], w[8], w[9], w[10], w[11], w[12])
            weather.save()

    return HttpResponse("<html><body><pre>Data: %s.</pre></body></html>" % json.dumps(data, indent=2))


def count_probability(requests):
    Probability.objects.all().delete()
    temp = Weather.objects.get_temp()
    humidity = Weather.objects.get_humidity()
    rainfall = Weather.objects.get_rainfall()
    wind = get_wind()
    wave = get_wave()

    for t in temp:
        prob = Probability.objects.create_prob(t, temp[t])
        prob.save()

    for h in humidity:
        prob = Probability.objects.create_prob(h, humidity[h])
        prob.save()

    for r in rainfall:
        prob = Probability.objects.create_prob(r, rainfall[r])
        prob.save()

    for w in wind:
        prob = Probability.objects.create_prob(w, wind[w])
        prob.save()

    for v in wave:
        prob = Probability.objects.create_prob(v, wave[v])
        prob.save()

    return HttpResponse()


def randomize():
    symbol = ""
    sym_from_wave = ""
    sym_from_wind = ""

    temp_prob = Weather.objects.get_temp()
    temp = [t for t in temp_prob]
    temp_weight = [temp_prob[t] for t in temp_prob]

    t = choices(temp, temp_weight)
    symbol = "|" + t[0]

    humid_prob = Weather.objects.get_humidity()
    humid = [h for h in humid_prob if symbol in h]
    humid_weight = [humid_prob[h] for h in humid]

    h = choices(humid, humid_weight)
    symbol = "|" + h[0]

    rainfall_prob = Weather.objects.get_rainfall()
    rainfall = [r for r in rainfall_prob if symbol in r]
    rainfall_weight = [rainfall_prob[r] for r in rainfall]

    r = choices(rainfall, rainfall_weight)
    symbol = r[0]

    wind_prob = get_wind()
    wind = [w for w in wind_prob]
    wind_weight = [wind_prob[w] for w in wind_prob]

    w = choices(wind, wind_weight)
    sym_from_wind = w[0]
    sym_from_wave = "|" + w[0]

    wave_prob = get_wave()
    wave = [v for v in wave_prob if sym_from_wave in v]
    wave_weight = [wave_prob[v] for v in wave]

    v = choices(wave, wave_weight)
    sym_from_wave = v[0]

    if re.match(r"^\-.+", symbol) and re.match(r"^\-.+", sym_from_wave) and re.match(r"^\-.+", sym_from_wind):
        return True

    return False


def count_maritim():
    conn = psycopg2.connect(os.environ['HEROKU_POSTGRESQL_NAVY_URL'])
    cur = conn.cursor()

    cur.execute("SELECT count(*) FROM maritim.stations")

    maritim = cur.fetchone()[0]

    return maritim


def get_wind():
    conn = psycopg2.connect(os.environ['HEROKU_POSTGRESQL_NAVY_URL'])
    cur = conn.cursor()

    cur.execute("SELECT count(*) FROM maritim.stations WHERE hi_wind <= 15")
    lo_wind = cur.fetchone()[0]
    cur.execute("SELECT count(*) FROM maritim.stations WHERE hi_wind > 15")
    hi_wind = cur.fetchone()[0]

    maritims = count_maritim()

    hi_wind_prob = hi_wind/maritims
    lo_wind_prob = lo_wind/maritims

    return dict({
        "-W": lo_wind_prob,
        "W": hi_wind_prob,
    })


def get_wave():
    conn = psycopg2.connect(os.environ['HEROKU_POSTGRESQL_NAVY_URL'])
    cur = conn.cursor()

    cur.execute(
        "SELECT count(*) FROM maritim.stations WHERE hi_wind <= 15 AND hi_waves > 2.5")
    hi_wave_lo_wind = cur.fetchone()[0]
    cur.execute(
        "SELECT count(*) FROM maritim.stations WHERE hi_wind <= 15 AND hi_waves <= 2.5")
    lo_wave_lo_wind = cur.fetchone()[0]
    cur.execute(
        "SELECT count(*) FROM maritim.stations WHERE hi_wind > 15 AND hi_waves > 2.5")
    hi_wave_hi_wind = cur.fetchone()[0]
    cur.execute(
        "SELECT count(*) FROM maritim.stations WHERE hi_wind > 15 AND hi_waves <= 2.5")
    lo_wave_hi_wind = cur.fetchone()[0]

    maritims = count_maritim()

    halb = hi_wave_lo_wind/maritims
    lalb = lo_wave_lo_wind/maritims
    hahb = hi_wave_hi_wind/maritims
    lahb = lo_wave_hi_wind/maritims

    return dict({
        "V|W": hahb,
        "V|-W": halb,
        "-V|W": lahb,
        "-V|-W": lalb,
    })
